# Makefile for the ctopy project
#
# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause

VERS=$(shell sed ctopy -n -e '/^version *= *"\(.*\)"/s//\1/p')

MANDIR=$(DESTDIR)/usr/share/man/man1
BINDIR=$(DESTDIR)/usr/bin

DOCS    = README COPYING NEWS ctopy.adoc ctopy.1 ctopy-logo.png
SOURCES = ctopy Makefile $(DOCS)

all: ctopy-$(VERS).tar.gz

# Note: to suppress the footers with timestamps being generated in HTML,
# we use "-a nofooter".
# To debug asciidoc problems, you may need to run "xmllint --nonet --noout --valid"
# on the intermediate XML that throws an error.
.SUFFIXES: .html .adoc .1

.adoc.1:
	asciidoctor -D. -a nofooter -b manpage $<
.adoc.html:
	asciidoctor -D. -a nofooter -a webfonts! $<

install: ctopy.1
	cp ctopy buildrpms $(BINDIR)
	gzip <ctopy.1 >$(MANDIR)/ctopy.1.gz

ctopy-$(VERS).tar.gz: $(SOURCES)
	@mkdir ctopy-$(VERS)
	@cp $(SOURCES) ctopy-$(VERS)
	@tar -czf ctopy-$(VERS).tar.gz ctopy-$(VERS)
	@rm -fr ctopy-$(VERS)

pylint:
	pylint --score=n ctopy

buildcheck:
	ctopy <testload.c >testload.py

check:
	@ctopy <testload.c >testload.py-trial
	@if diff -u testload.py testload.py-trial; \
	then echo "Test succeeded"; \
	else echo "Test failed"; fi
	@rm -f testload.py-trial

clean:
	rm -f *.html *.1 *.pyc testload.py-trial typescript *.tar.gz

dist: ctopy-$(VERS).tar.gz

NEWSVERSION=$(shell sed -n <NEWS '/^[0-9]/s/:.*//p' | head -1)

version:
	@echo $(VERS) $(NEWSVERSION)

release: ctopy-$(VERS).tar.gz ctopy.html
	@[ $(VERS) = $(NEWSVERSION) ] || { echo "Version mismatch!"; exit 1; }
	shipper version=$(VERS) | sh -e -x

refresh: ctopy.html
	@[ $(VERS) = $(NEWSVERSION) ] || { echo "Version mismatch!"; exit 1; }
	shipper -N -w version=$(VERS) | sh -e -x


